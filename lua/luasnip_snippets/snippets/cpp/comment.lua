local snippets = {
	s(
		{ trig = 'test', name = 'test', dscr = '' },
		fmt(
			[[
			// \test <<<
			{}
			// \test >>>
			]],
			{ i(0, '') }
		)
	),
}

local autosnippets = {}

if require('luasnip_snippets.config').option.cpp.comment then
	return snippets, autosnippets
else
	return {}, {}
end
-- vi: set noexpandtab:
