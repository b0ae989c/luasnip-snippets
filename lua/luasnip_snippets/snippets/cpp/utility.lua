local snippets = {
	s(
		{ trig = 'ns', name = 'namespace', dscr = 'namespace' },
		fmta(
			[[
			namespace <1> {
				<>
			}
			]],
			{ i(1, ''), i(0, '') }
		)
	),
}

local autosnippets = {}

if require('luasnip_snippets.config').option.cpp.utility then
	return snippets, autosnippets
else
	return {}, {}
end
-- vi: set noexpandtab:
