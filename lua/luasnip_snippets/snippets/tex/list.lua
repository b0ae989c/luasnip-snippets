local util = require('luasnip_snippets.util')

local snippets = {
	s(
		{ trig = 'list', name = 'itemize', dscr = '' },
		fmta(
			[[
			\begin{itemize}
				\item <>
			\end{itemize}
			]],
			{ i(0, '') }
		)
	),
	s(
		{ trig = 'enum', name = 'enumerate', dscr = '' },
		fmta(
			[[
			\begin{enumerate}
				\item <>
			\end{enumerate}
			]],
			{ i(0, '') }
		)
	),
	s(
		{ trig = 'outline', name = 'outline', dscr = '' },
		fmta(
			[[
			\begin{outline}[enumerate]
				\1 <>
			\end{outline}
			]],
			{ i(0, '') }
		)
	),
	s({
		trig = 'it',
		name = 'item',
		dscr = '',
		condition = function()
			return util.check_tex_environment({ 'itemize', 'enumerate' })
		end,
		show_condition = function()
			return util.check_tex_environment({ 'itemize', 'enumerate' })
		end,
	}, fmta([[\item <>]], { i(0, '') })),
}

local autosnippets = {}

if require('luasnip_snippets.config').option.tex.list then
	return snippets, autosnippets
else
	return {}, {}
end
-- vi: set noexpandtab:
