local snippets = {
	s(
		{ trig = 'eqn', name = 'equation', dscr = '' },
		fmta(
			[[
			\begin{equation}
				<>
			\end{equation}
			]],
			{ i(0, '') }
		)
	),
	s(
		{ trig = 'seqn', name = 'subequations', dscr = '' },
		fmta(
			[[
			\begin{subequations}
				\begin{align}
					<>
				\end{align}
			\end{subequations}
			]],
			{ i(0, '') }
		)
	),
	s(
		{ trig = 'ali', name = 'align', dscr = '' },
		fmta(
			[[
			\begin{aligned}
				<>
			\end{aligned}
			]],
			{ i(0, '') }
		)
	),
}

local autosnippets = {}

if require('luasnip_snippets.config').option.tex.equation then
	return snippets, autosnippets
else
	return {}, {}
end
-- vi: set noexpandtab:
