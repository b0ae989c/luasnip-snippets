local snippets = {
	s(
		{ trig = 'mat', name = 'NiceMatrix', dscr = '' },
		fmta(
			[[
			\begin{NiceMatrix}
				<>
			\end{NiceMatrix}
			]],
			{ i(0, '') }
		)
	),
	s(
		{ trig = 'bmat', name = 'bNiceMatrix', dscr = '' },
		fmta(
			[[
			\begin{bNiceMatrix}
				<>
			\end{bNiceMatrix}
			]],
			{ i(0, '') }
		)
	),
	s(
		{ trig = 'pmat', name = 'pNiceMatrix', dscr = '' },
		fmta(
			[[
			\begin{pNiceMatrix}
				<>
			\end{pNiceMatrix}
			]],
			{ i(0, '') }
		)
	),
	s(
		{ trig = 'vmat', name = 'vNiceMatrix', dscr = '' },
		fmta(
			[[
			\begin{vNiceMatrix}
				<>
			\end{vNiceMatrix}
			]],
			{ i(0, '') }
		)
	),
	s(
		{ trig = 'Bmat', name = 'BNiceMatrix', dscr = '' },
		fmta(
			[[
			\begin{BNiceMatrix}
				<1>
			\end{BNiceMatrix}
			]],
			{ i(1, '') }
		)
	),
	s(
		{ trig = 'Vmat', name = 'VNiceMatrix', dscr = '' },
		fmta(
			[[
			\begin{VNiceMatrix}
				<>
			\end{VNiceMatrix}
			]],
			{ i(0, '') }
		)
	),
}

local autosnippets = {}

if require('luasnip_snippets.config').option.tex.matrix then
	return snippets, autosnippets
else
	return {}, {}
end
-- vi: set noexpandtab:
