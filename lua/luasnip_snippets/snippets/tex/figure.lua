local snippets = {
	s(
		{ trig = 'fig', name = 'figure', dscr = '' },
		fmta(
			[[
			\begin{figure}[<1>]
				\centering
				\includegraphics[<2>]{<3>}
				\caption{<4>}
				\label{<>}
			\end{figure}
			]],
			{ i(1, ''), i(2, ''), i(3, ''), i(4, ''), i(0, '') }
		)
	),
	s(
		{ trig = 'sfig', name = 'subfigure', dscr = '' },
		fmta(
			[[
			\begin{subfigure}[b]{<1>\textwidth}
				\centering
				\includegraphics[width=\textwidth]{<2>}
				\caption{<3>}
				\label{<>}
			\end{subfigure}
			\hfill
			]],
			{ i(1, ''), i(2, ''), i(3, ''), i(0, '') }
		)
	),
}

local autosnippets = {}

if require('luasnip_snippets.config').option.tex.figure then
	return snippets, autosnippets
else
	return {}, {}
end
-- vi: set noexpandtab:
